package com.vytrykhovskyi.readwritelock;

public class Lock implements LockCustom {

    private int lockHoldCount;
    private long IdOfThreadCurrentlyHoldingLock;

    Lock() {
        lockHoldCount = 0;
    }

    @Override
    public synchronized void lock() {
        if (lockHoldCount == 0) {
            lockHoldCount++;
            IdOfThreadCurrentlyHoldingLock = Thread.currentThread().getId();
        } else if (lockHoldCount > 0
                && IdOfThreadCurrentlyHoldingLock == Thread.currentThread().getId()) {
            lockHoldCount++;
        } else {
            try {
                wait();
                lockHoldCount++;
                IdOfThreadCurrentlyHoldingLock = Thread.currentThread().getId();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void unlock() {
        if (lockHoldCount == 0) {
            throw new IllegalMonitorStateException();
        }
        lockHoldCount--;
        if (lockHoldCount == 0) {
            notify();
        }
    }
}
