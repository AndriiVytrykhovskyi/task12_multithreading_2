package com.vytrykhovskyi.readwritelock;

public interface LockCustom {
    void lock();

    void unlock();
}
