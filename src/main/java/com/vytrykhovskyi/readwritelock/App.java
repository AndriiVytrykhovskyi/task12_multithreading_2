package com.vytrykhovskyi.readwritelock;

public class App {
    public static void main(String[] args) {
        LockCustom LockCustom = new Lock();
        MyRunnable myRunnable = new MyRunnable(LockCustom);
        new Thread(myRunnable, "Thread-1").start();
        new Thread(myRunnable, "Thread-2").start();
    }
}
