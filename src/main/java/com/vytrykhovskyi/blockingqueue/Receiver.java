package com.vytrykhovskyi.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Receiver extends Thread {
    private BlockingQueue blockingQueue = null;

    public Receiver(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                // Blocks until characters are there:
                System.out.println("Read: " + (char) blockingQueue.take());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
