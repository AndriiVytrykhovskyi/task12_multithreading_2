package com.vytrykhovskyi.blockingqueue;

import java.util.concurrent.*;

public class App {
    public static void main(String[] args) throws Exception {
        BlockingQueue bq = new ArrayBlockingQueue(10);
        Sender sender = new Sender(bq);
        Receiver receiver = new Receiver(bq);

        ExecutorService service = Executors.newCachedThreadPool();

        try {
            service.execute(receiver);
            service.execute(sender);
        } finally {
            service.shutdown();
        }
    }
}
