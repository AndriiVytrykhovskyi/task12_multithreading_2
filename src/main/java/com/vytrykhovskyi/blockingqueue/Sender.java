package com.vytrykhovskyi.blockingqueue;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Sender extends Thread {
    private BlockingQueue blockingQueue;
    private Random rand = new Random();

    public Sender(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        while (true) {
            for (char c = 'A'; c <= 'z'; c++) {
                try {
                    blockingQueue.put(c);
                    sleep(rand.nextInt(500));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}