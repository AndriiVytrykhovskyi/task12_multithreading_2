import com.vytrykhovskyi.explicitlock.MassiveOperations;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) {
        MassiveOperations mo = new MassiveOperations(11);
        ExecutorService service =
                Executors.newCachedThreadPool();
        try {
            for (int i = 0; i < 5; i++) {
                service.execute(mo::firstMethod);
                service.execute(mo::secondMethod);
                service.execute(mo::thirdMethod);
            }
        } finally {
            service.shutdown();
        }
    }
}
