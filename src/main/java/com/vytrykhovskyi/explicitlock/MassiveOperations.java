package com.vytrykhovskyi.explicitlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MassiveOperations {
    private int mas[] = new int[MAX_SIZE];
    private final static int MAX_SIZE = 10;

    private Lock lock = new ReentrantLock();

    public MassiveOperations(int second) {
        mas[1] = second;
    }

    public void firstMethod() {
        lock.lock();
        try {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] = i;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }

    public void secondMethod() {
        lock.lock();
        try {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] *= 2;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }

    public void thirdMethod() {
        lock.lock();
        try {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] *= 10;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }
}
